@echo off
cd ..
set REPO_DIR=%CD%
mkdir \home\jboss
cd \home\jboss
mklink /D source %REPO_DIR%
cd %REPO_DIR%\bin
for /r %%i in (*.sh) do git update-index --chmod=+x %%i