# Shibboleth IdP v3 Installation Guide on OpenShift v3 #

This is the guide on how to install and configure Shibboleth Identity Provider v3.2.1 on an OpenShift v3 container.

### Purpose ###
This repository contains files from the official Shibboleth Identity Provider download package, as well as installation scripts that are structured and configured in a way that can be built, deployed, and configured in an OpenShift v3 (Docker- and Kubernetes-based) container.

### Version ###
Shibboleth IdP v3.2.1  
OpenShift: v3 (Docker- and Kubernetes-based)

## Installation Procedures ##

### System Requirements and Dependencies ###

Please refer to the [Shibboleth IdP V3 wiki](https://wiki.shibboleth.net/confluence/display/IDP30/SystemRequirements) for the official system requirements.

The following are used at the time of writing:
  
* **OpenJDK 8** (included in OpenShift container image: *jboss-webserver-3/webserver30-tomcat8-openshift*)
* **JBoss Web Server 3.0 - Tomcat v8** (included in OpenShift container image: *jboss-webserver-3/webserver30-tomcat8-openshift*)

### Preinstallation Tasks ###

### OpenShift v3 Container Setup ###

### Deployment ###

### Configuration ###

### Updates and Maintenance ###

## Who do I talk to? ##
* Repo owner or admin: **[Mark Buising](mailto:mlbuising.app.dev@gmail.com)**